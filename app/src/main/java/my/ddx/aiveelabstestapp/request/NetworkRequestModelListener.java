package my.ddx.aiveelabstestapp.request;

/**
 * a
 * Created by deler on 14.09.15.
 */
public interface NetworkRequestModelListener {
    void onStartLoad();

    void onStopLoad();

    void onDataLoaded(String text);

    void onLoadDataFail(String localizedMessage);
}
