package my.ddx.aiveelabstestapp.request.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.converter.GsonConverter;

/**
 * a
 * Created by deler on 14.09.15.
 */
public class RequestServiceAdapter {

    private final RequestService service;
    private final String SERVER_ADDRESS = "http://sb.cidertv.com";

    public RequestServiceAdapter() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(SERVER_ADDRESS)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new AndroidLog("RequestServiceAdapter"))
                .setConverter(new GsonConverter(gson));

        RestAdapter restAdapter = builder.build();

        service = restAdapter.create(RequestService.class);
    }

    public RequestService getService() {
        return service;
    }
}
