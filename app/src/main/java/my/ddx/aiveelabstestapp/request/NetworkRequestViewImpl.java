package my.ddx.aiveelabstestapp.request;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import my.ddx.aiveelabstestapp.R;

/**
 * a
 * Created by deler on 14.09.15.
 */
class NetworkRequestViewImpl implements NetworkRequestView {
    @Bind(R.id.text)
    TextView text;
    private NetworkRequestViewListener listener;
    private Menu optionsMenu;
    private boolean progressOn;

    @Override
    public void setListener(NetworkRequestViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Activity activity) {
        ButterKnife.bind(this, activity);
    }

    @Override
    public void onDestroy() {
        listener = null;
    }

    @Override
    public void setText(String text) {
        this.text.setText(text);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        setRefreshActionButtonState(progressOn);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                if (listener != null) {
                    listener.onBackPressed();
                }
                return true;
            case R.id.action_refresh:
                if (listener != null) {
                    listener.onRefreshPressed();
                }
                return true;
        }
        return false;
    }

    private void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu.findItem(R.id.action_refresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    @Override
    public void onStartProgress() {
        progressOn = true;
        setRefreshActionButtonState(true);
    }

    @Override
    public void onStopProgress() {
        progressOn = false;
        setRefreshActionButtonState(false);
    }
}
