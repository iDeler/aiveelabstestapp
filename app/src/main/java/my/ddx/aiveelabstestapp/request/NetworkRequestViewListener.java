package my.ddx.aiveelabstestapp.request;

/**
 * a
 * Created by deler on 14.09.15.
 */
public interface NetworkRequestViewListener {

    void onBackPressed();

    void onRefreshPressed();
}
