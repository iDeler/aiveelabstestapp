package my.ddx.aiveelabstestapp.request;

import android.content.Context;

/**
 * a
 * Created by deler on 14.09.15.
 */
public interface NetworkRequestModel {
    void onCreate(Context context);

    void setListener(NetworkRequestModelListener listener);

    void onDestroy();

    void refreshData();
}
