package my.ddx.aiveelabstestapp.request.service;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;

/**
 * a
 * Created by deler on 14.09.15.
 */
public interface RequestService {

    @POST("/api/v1/droid/rulez")
    void postRequestWantJob(@Body RequestWantJob requestWantJob, Callback<ResponseWantJob> callback);

    class RequestWantJob {
        public String reason;

        public RequestWantJob() {
            reason = "i_want_this_job";
        }
    }

    class ResponseWantJob {
        public String encodedMessage;
    }
}
