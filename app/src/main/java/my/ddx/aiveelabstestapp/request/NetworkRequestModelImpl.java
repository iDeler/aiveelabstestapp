package my.ddx.aiveelabstestapp.request;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;

import my.ddx.aiveelabstestapp.request.service.RequestService;
import my.ddx.aiveelabstestapp.request.service.RequestServiceAdapter;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * a
 * Created by deler on 14.09.15.
 */
public class NetworkRequestModelImpl implements NetworkRequestModel {
    private NetworkRequestModelListener listener;
    private RequestServiceAdapter serviceAdapter;

    @Override
    public void onCreate(Context context) {
        serviceAdapter = new RequestServiceAdapter();
    }

    @Override
    public void setListener(NetworkRequestModelListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDestroy() {
        listener = null;
    }

    @Override
    public void refreshData() {
        loadData();
    }

    private void loadData() {
        if (listener != null) {
            listener.onStartLoad();
        }
        serviceAdapter.getService().postRequestWantJob(new RequestService.RequestWantJob(), new Callback<RequestService.ResponseWantJob>() {
            @Override
            public void success(RequestService.ResponseWantJob responseWantJob, Response response) {
                onDataLoadedSuccess(responseWantJob);
            }

            @Override
            public void failure(RetrofitError error) {
                onDataLoadedFail(error);
            }
        });
    }

    private void onDataLoadedFail(RetrofitError error) {
        if (listener != null) {
            listener.onLoadDataFail(error.getLocalizedMessage());
            listener.onStopLoad();
        }
    }

    private void onDataLoadedSuccess(RequestService.ResponseWantJob responseWantJob) {
        String text = null;
        String error = null;
        try {
            byte[] data = Base64.decode(responseWantJob.encodedMessage, Base64.DEFAULT);
            text = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            error = e.getLocalizedMessage();
        }
        if (TextUtils.isEmpty(text)) {
            if (listener != null) {
                listener.onLoadDataFail(TextUtils.isEmpty(error) ? "Unknow Error" : error);
            }
        } else {
            if (listener != null) {
                listener.onDataLoaded(text);
            }
        }

        if (listener != null) {
            listener.onStopLoad();
        }
    }
}
