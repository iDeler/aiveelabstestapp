package my.ddx.aiveelabstestapp.request;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * a
 * Created by deler on 14.09.15.
 */
public interface NetworkRequestView {
    void setListener(NetworkRequestViewListener listener);

    void onCreate(Activity activity);

    void onDestroy();

    void setText(String text);

    void onCreateOptionsMenu(Menu menu);

    boolean onOptionsItemSelected(MenuItem item);

    void onStartProgress();

    void onStopProgress();
}
