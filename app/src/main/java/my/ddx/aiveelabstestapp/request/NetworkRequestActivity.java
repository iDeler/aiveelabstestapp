package my.ddx.aiveelabstestapp.request;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import my.ddx.aiveelabstestapp.R;

public class NetworkRequestActivity extends AppCompatActivity {

    private NetworkRequestView requestView;
    private NetworkRequestModel requestModel;

    public static void start(Context context) {
        Intent starter = new Intent(context, NetworkRequestActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_network_request);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        requestView = new NetworkRequestViewImpl();
        requestView.onCreate(this);
        requestView.setListener(new NetworkRequestViewListener() {
            @Override
            public void onBackPressed() {
                finish();
            }

            @Override
            public void onRefreshPressed() {
                requestModel.refreshData();
            }
        });

        requestModel = new NetworkRequestModelImpl();
        requestModel.onCreate(this);
        requestModel.setListener(new NetworkRequestModelListener() {

            @Override
            public void onStartLoad() {
                requestView.onStartProgress();
            }

            @Override
            public void onStopLoad() {
                requestView.onStopProgress();
            }

            @Override
            public void onDataLoaded(String text) {
                requestView.setText(text);
            }

            @Override
            public void onLoadDataFail(String localizedMessage) {
                Toast.makeText(NetworkRequestActivity.this, "Load Data Fail:" + localizedMessage, Toast.LENGTH_SHORT).show();
                requestView.setText(localizedMessage);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        requestModel.refreshData();
    }

    @Override
    protected void onDestroy() {
        requestView.onDestroy();
        requestView = null;
        requestModel.onDestroy();
        requestModel = null;
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_network_request, menu);
        requestView.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return requestView.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


}
