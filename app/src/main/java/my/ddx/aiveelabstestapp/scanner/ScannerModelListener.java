package my.ddx.aiveelabstestapp.scanner;

import java.util.List;

import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;

/**
 * Created by deler on 13.09.15.
 */
public interface ScannerModelListener {
    void onStartScan();

    void onStopScan();

    void onUpdateDiscovered(List<Metadata> discovered);
}
