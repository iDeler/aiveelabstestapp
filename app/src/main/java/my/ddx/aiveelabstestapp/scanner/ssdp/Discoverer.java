package my.ddx.aiveelabstestapp.scanner.ssdp;

/**
 * a
 * Created by deler on 13.09.15.
 */
abstract public class Discoverer {

    private DiscoveryCallback discoveryCallback;

    public abstract void startScanImpl();

    public abstract void stopScanImpl();

    public void startScan() {
        startScanImpl();
    }

    public void stopScan() {
        stopScanImpl();
    }

    public void restartScan() {
        stopScan();
        startScan();
    }

    public void setCallback(DiscoveryCallback discoveryCallback) {
        this.discoveryCallback = discoveryCallback;
    }

    protected void report(Metadata metadata) {
        discoveryCallback.onDiscovered(metadata);
    }

    public interface DiscoveryCallback {
        void onDiscovered(Metadata metadata);
    }
}
