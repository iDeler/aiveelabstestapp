package my.ddx.aiveelabstestapp.scanner;

import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;

/**
 * a
 * Created by deler on 13.09.15.
 */
public interface ScannerViewListener {
    void onDiscoveredItemSelected(Metadata metadata);

    void onBackPressed();

    void onScanPressed();
}
