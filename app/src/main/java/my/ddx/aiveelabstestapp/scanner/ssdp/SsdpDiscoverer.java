/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package my.ddx.aiveelabstestapp.scanner.ssdp;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

/**
 * This class discovers Physical Web URI/URLs over SSDP.
 */

public class SsdpDiscoverer extends Discoverer implements Ssdp.SsdpCallback {
    private static final String TAG = "SsdpDiscoverer";
    private static final String PHYSICAL_WEB_SSDP_TYPE = "urn:physical-web-org:device:Basic:1";
    private static final String PHYSICAL_UPNP_SSDP_TYPE = "urn:schemas-upnp-org:device:Basic:1";
    private static final String CONTENT_DIRECTORY_SSDP_TYPE = "urn:schemas-upnp-org:service:ContentDirectory:1";
    private static final String AV_TRANSPORT_SSDP_TYPE = "urn:schemas-upnp-org:service:AVTransport:1";
    private static final String OPENHOME_PRODUCT_SSDP_TYPE = "urn:av-openhome-org:service:Product:1";
    private Context context;
    private Thread thread;
    private Ssdp ssdp;

    public SsdpDiscoverer(Context context) {
        this.context = context;
    }

    public void startScanImpl() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getSsdp().start(null);
                    Thread.sleep(200);
                    getSsdp().search(PHYSICAL_WEB_SSDP_TYPE);
                    getSsdp().search(PHYSICAL_UPNP_SSDP_TYPE);
                    getSsdp().search(CONTENT_DIRECTORY_SSDP_TYPE);
                    getSsdp().search(AV_TRANSPORT_SSDP_TYPE);
                    getSsdp().search(OPENHOME_PRODUCT_SSDP_TYPE);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }).start();
    }

    public void stopScanImpl() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getSsdp().stop();
                } catch (IOException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }).start();
    }

    public synchronized Ssdp getSsdp() throws IOException {
        if (ssdp == null) {
            ssdp = new Ssdp(this);
        }
        return ssdp;
    }

    @Override
    public void onSsdpMessageReceived(final SsdpMessage ssdpMessage) {
        final String st = ssdpMessage.get("ST");
//        if (url != null && PHYSICAL_WEB_SSDP_TYPE.equals(st)) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Metadata metadata = createMetadata(ssdpMessage);
                report(metadata);
            }
        }).start();
//        }
    }

    private Metadata createMetadata(SsdpMessage ssdpMessage) {
        return new Metadata(ssdpMessage.getAddress().getHostAddress(), ssdpMessage.getAddress().getHostName(), ssdpMessage.get("LOCATION"), ssdpMessage.getHeaders());
    }
}
