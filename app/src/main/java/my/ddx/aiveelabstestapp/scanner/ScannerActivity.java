package my.ddx.aiveelabstestapp.scanner;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import my.ddx.aiveelabstestapp.R;
import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;

public class ScannerActivity extends AppCompatActivity {
    private static final String TAG = ScannerActivity.class.getSimpleName();

    private ScannerModel scannerModel;
    private ScannerView scannerView;

    public static void start(Context context) {
        Intent starter = new Intent(context, ScannerActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scanner);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        scannerView = new ScannerViewImpl();
        scannerView.onCreate(this);
        scannerView.setListener(new ScannerViewListener() {
            @Override
            public void onDiscoveredItemSelected(Metadata metadata) {
                //TODO: something
                Toast.makeText(ScannerActivity.this, "onDiscoveredItemSelected:" + metadata, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onBackPressed() {
                finish();
            }

            @Override
            public void onScanPressed() {
                scannerModel.startScan();
            }
        });

        scannerModel = new ScannerModelImpl();
        scannerModel.onCreate(this);

        scannerModel.setListener(new ScannerModelListener() {

            @Override
            public void onStartScan() {
                scannerView.onStartScan();
            }

            @Override
            public void onStopScan() {
                scannerView.onStopScan();
            }

            @Override
            public void onUpdateDiscovered(List<Metadata> discovered) {
                scannerView.setDiscovered(discovered);
            }
        });
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        scannerModel.startScan();
    }

    @Override
    protected void onDestroy() {
        scannerModel.onDestroy();
        scannerView.onDestroy();
        scannerModel = null;
        scannerView = null;
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scanner, menu);
        scannerView.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return scannerView.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

}
