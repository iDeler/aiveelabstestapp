package my.ddx.aiveelabstestapp.scanner;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import my.ddx.aiveelabstestapp.R;
import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;
import my.ddx.aiveelabstestapp.utils.RecyclerItemClickListener;
import my.ddx.aiveelabstestapp.utils.SimpleDividerItemDecoration;

/**
 * a
 * Created by deler on 13.09.15.
 */
class ScannerViewImpl implements ScannerView {
    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    private ScannerViewListener listener;
    private List<Metadata> discovered = new ArrayList<>();
    private ScannerViewAdapter adapter;
    private Menu optionsMenu;
    private boolean scanning;

    @Override
    public void setListener(ScannerViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void setDiscovered(List<Metadata> discovered) {
        this.discovered = discovered;
        adapter.setDiscovered(discovered);
    }

    @Override
    public void onCreate(Activity activity) {
        ButterKnife.bind(this, activity);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(activity));
        adapter = new ScannerViewAdapter(discovered);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(activity, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (listener != null) {
                            Metadata metadata = discovered.get(position);
                            listener.onDiscoveredItemSelected(metadata);
                        }
                    }
                })
        );
    }

    @Override
    public void onDestroy() {
        listener = null;
    }

    @Override
    public void onStartScan() {
        scanning = true;
        setRefreshActionButtonState(true);
    }

    @Override
    public void onStopScan() {
        scanning = false;
        setRefreshActionButtonState(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu) {
        optionsMenu = menu;
        setRefreshActionButtonState(scanning);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                if (listener != null) {
                    listener.onBackPressed();
                }
                return true;
            case R.id.action_scan:
                if (listener != null) {
                    listener.onScanPressed();
                }
                return true;
        }
        return false;
    }

    private void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu.findItem(R.id.action_scan);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    private static class ScannerViewAdapter extends RecyclerView.Adapter<DiscoveredCellViewHolder> {

        private List<Metadata> discovered;

        public ScannerViewAdapter(List<Metadata> discovered) {
            this.discovered = discovered;
        }

        @Override
        public DiscoveredCellViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_scanner_dicovered, viewGroup, false);
            return new DiscoveredCellViewHolder(v);
        }

        @Override
        public void onBindViewHolder(DiscoveredCellViewHolder discoveredCellViewHolder, int i) {
            Metadata metadata = discovered.get(i);
            discoveredCellViewHolder.setup(metadata);
        }

        @Override
        public int getItemCount() {
            return discovered.size();
        }

        public void setDiscovered(List<Metadata> discovered) {
            this.discovered = discovered;
            notifyDataSetChanged();
        }
    }

    static class DiscoveredCellViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.host_name_textView)
        TextView hostNameTextView;
        @Bind(R.id.address_textView)
        TextView addressTextView;

        private Metadata metadata;

        public DiscoveredCellViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void setup(Metadata metadata) {
            this.metadata = metadata;

            hostNameTextView.setText(metadata.hostName);
            addressTextView.setText(metadata.address);
        }
    }
}
