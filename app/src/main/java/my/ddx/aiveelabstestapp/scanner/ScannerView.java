package my.ddx.aiveelabstestapp.scanner;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;

/**
 * a
 * Created by deler on 13.09.15.
 */
public interface ScannerView {
    void setListener(ScannerViewListener listener);

    void setDiscovered(List<Metadata> discovered);

    void onCreate(Activity activity);

    void onDestroy();

    void onStartScan();

    void onStopScan();

    void onCreateOptionsMenu(Menu menu);

    boolean onOptionsItemSelected(MenuItem item);
}
