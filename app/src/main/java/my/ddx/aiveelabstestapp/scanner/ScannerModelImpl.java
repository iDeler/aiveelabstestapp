package my.ddx.aiveelabstestapp.scanner;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import my.ddx.aiveelabstestapp.scanner.ssdp.Discoverer;
import my.ddx.aiveelabstestapp.scanner.ssdp.Metadata;
import my.ddx.aiveelabstestapp.scanner.ssdp.SsdpDiscoverer;

/**
 * a
 * Created by deler on 13.09.15.
 */
public class ScannerModelImpl implements ScannerModel, Discoverer.DiscoveryCallback {
    private static final String TAG = ScannerModelImpl.class.getSimpleName();
    private static final long TIMEOUT = 10000;

    private Discoverer discoverer;
    private Handler handler;
    private ScannerModelListener listener;
    private Runnable timeout = new Runnable() {
        @Override
        public void run() {
            stopScan();
        }
    };
    private List<Metadata> discovered = new ArrayList<>();

    @Override
    public void setListener(ScannerModelListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Context context) {
        handler = new Handler();

        discoverer = new SsdpDiscoverer(context);
        discoverer.setCallback(this);
    }

    @Override
    public void onDiscovered(final Metadata metadata) {
        Log.d(TAG, metadata.toString());
        handler.post(new Runnable() {
            @Override
            public void run() {
                discovered.add(metadata);
                if (listener != null) {
                    listener.onUpdateDiscovered(getDiscovered());
                }
            }
        });
    }

    public List<Metadata> getDiscovered() {
        return new ArrayList<>(discovered);
    }

    @Override
    public void startScan() {
        discovered.clear();
        discoverer.restartScan();
        handler.postDelayed(timeout, TIMEOUT);

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.onStartScan();
                }
            }
        });
    }

    @Override
    public void stopScan() {
        handler.removeCallbacks(timeout);
        discoverer.stopScan();

        handler.post(new Runnable() {
            @Override
            public void run() {
                if (listener != null) {
                    listener.onStopScan();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        listener = null;
        stopScan();
        handler = null;
    }
}
