package my.ddx.aiveelabstestapp.scanner.ssdp;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * a
 * Created by deler on 13.09.15.
 */
public class SsdpMessage {
    public static final int TYPE_SEARCH = 0;
    public static final int TYPE_NOTIFY = 1;
    public static final int TYPE_FOUND = 2;
    private static final String NL = "\r\n";
    private static final String FIRST_LINE[] = {
            Ssdp.TYPE_M_SEARCH + " * HTTP/1.1",
            Ssdp.TYPE_NOTIFY + " * HTTP/1.1",
            "HTTP/1.1 " + Ssdp.TYPE_200_OK
    };
    private InetAddress address;
    private int mType;
    private Map<String, String> mHeaders;

    public SsdpMessage(int type) {
        this.mType = type;
    }

    public SsdpMessage(String txt) {
        String lines[] = txt.split(NL);
        String line = lines[0].trim();
        if (line.startsWith(Ssdp.TYPE_M_SEARCH)) {
            this.mType = TYPE_SEARCH;
        } else if (line.startsWith(Ssdp.TYPE_NOTIFY)) {
            this.mType = TYPE_NOTIFY;
        } else {
            this.mType = TYPE_FOUND;
        }
        for (int i = 1; i < lines.length; i++) {
            line = lines[i].trim();
            int index = line.indexOf(':');
            if (index > 0) {
                String key = line.substring(0, index).trim();
                String value = line.substring(index + 1).trim();
                getHeaders().put(key, value);
            }
        }
    }

    public InetAddress getAddress() {
        return address;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public Map<String, String> getHeaders() {
        if (mHeaders == null) {
            mHeaders = new HashMap<>();
        }
        return mHeaders;
    }

    public int getType() {
        return mType;
    }

    public String get(String key) {
        return getHeaders().get(key);
    }

    public String put(String key, String value) {
        return getHeaders().put(key, value);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(FIRST_LINE[this.mType]).append(NL);
        for (Map.Entry<String, String> entry : getHeaders().entrySet()) {
            builder.append(entry.getKey())
                    .append(": ")
                    .append(entry.getValue())
                    .append(NL);
        }
        builder.append(NL);
        return builder.toString();
    }
}
