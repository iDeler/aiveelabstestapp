package my.ddx.aiveelabstestapp.scanner;

import android.content.Context;

/**
 * Created by deler on 13.09.15.
 */
public interface ScannerModel {
    void setListener(ScannerModelListener listener);

    void onCreate(Context context);

    void startScan();

    void stopScan();

    void onDestroy();
}
