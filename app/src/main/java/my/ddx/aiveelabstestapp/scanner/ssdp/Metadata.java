package my.ddx.aiveelabstestapp.scanner.ssdp;

import java.util.Map;

/**
 * a
 * Created by deler on 13.09.15.
 */
public class Metadata {
    public final String address;
    public final String hostName;
    public final String url;
    public final Map<String, String> headers;

    public Metadata(String address, String hostName, String url, Map<String, String> headers) {
        this.address = address;
        this.hostName = hostName;
        this.url = url;
        this.headers = headers;
    }


    @Override
    public String toString() {
        return "Metadata{" +
                "address='" + address + '\'' +
                ", hostName='" + hostName + '\'' +
                ", url='" + url + '\'' +
                ", headers=" + headers +
                '}';
    }
}
