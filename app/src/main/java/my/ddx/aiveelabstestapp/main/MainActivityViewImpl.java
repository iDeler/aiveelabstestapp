package my.ddx.aiveelabstestapp.main;

import android.app.Activity;
import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import my.ddx.aiveelabstestapp.R;

/**
 * a
 * Created by deler on 13.09.15.
 */
class MainActivityViewImpl implements MainActivityView {
    @Bind(R.id.ssdp_scanner_button)
    Button ssdpScannerButton;
    @Bind(R.id.plume_effect_button)
    Button plumeEffectButton;
    @Bind(R.id.network_request_button)
    Button networkRequestButton;

    MainActivityViewListener listener;

    @Override
    public void setListener(MainActivityViewListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCreate(Activity activity) {
        ButterKnife.bind(this, activity);

        ssdpScannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onOpenSSDPScanner();
                }
            }
        });

        plumeEffectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onOpenPlumeEffect();
                }
            }
        });

        networkRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onOpenNetworkRequest();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        listener = null;
    }
}
