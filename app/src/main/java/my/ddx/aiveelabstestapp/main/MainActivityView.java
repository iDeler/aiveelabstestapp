package my.ddx.aiveelabstestapp.main;

import android.app.Activity;

/**
 * a
 * Created by deler on 13.09.15.
 */
public interface MainActivityView {
    void setListener(MainActivityViewListener listener);

    void onCreate(Activity activity);

    void onDestroy();
}
