package my.ddx.aiveelabstestapp.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import my.ddx.aiveelabstestapp.R;
import my.ddx.aiveelabstestapp.plume.PlumeEffectActivity;
import my.ddx.aiveelabstestapp.request.NetworkRequestActivity;
import my.ddx.aiveelabstestapp.scanner.ScannerActivity;

public class MainActivity extends AppCompatActivity implements MainActivityViewListener {

    private MainActivityView mainActivityView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mainActivityView = new MainActivityViewImpl();
        mainActivityView.setListener(this);
        mainActivityView.onCreate(this);
    }

    @Override
    protected void onDestroy() {
        mainActivityView.onDestroy();
        mainActivityView = null;
        super.onDestroy();
    }

    @Override
    public void onOpenSSDPScanner() {
        ScannerActivity.start(this);
    }

    @Override
    public void onOpenPlumeEffect() {
        PlumeEffectActivity.start(this);
    }

    @Override
    public void onOpenNetworkRequest() {
        NetworkRequestActivity.start(this);
    }

}
