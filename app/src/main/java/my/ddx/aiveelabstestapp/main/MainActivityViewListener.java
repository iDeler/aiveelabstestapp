package my.ddx.aiveelabstestapp.main;

/**
 * a
 * Created by deler on 13.09.15.
 */
public interface MainActivityViewListener {
    void onOpenSSDPScanner();

    void onOpenPlumeEffect();

    void onOpenNetworkRequest();

}
