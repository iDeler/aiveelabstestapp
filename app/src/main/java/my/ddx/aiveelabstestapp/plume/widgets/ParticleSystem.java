package my.ddx.aiveelabstestapp.plume.widgets;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * a
 * Created by deler on 13.09.15.
 */
public class ParticleSystem {
    private Particle particles[];

    private Bitmap bitmap;

    public ParticleSystem(int maxParticlesCount, long maxLifeTime, Bitmap bitmap) {
        this.bitmap = bitmap;
        particles = new Particle[maxParticlesCount];

        for (int i = 0; i < maxParticlesCount; i++) {
            particles[i] = new Particle(maxLifeTime, bitmap);
        }
    }

    public void setParticle(int x, int y) {
        Particle particleWithMinLifeTime = null;
        for (Particle particle : particles) {
            if (!particle.isLife()) {
                particle.setLife(x, y);
                return;
            } else {
                if (particleWithMinLifeTime != null) {
                    if (particleWithMinLifeTime.getLifeTime() > particle.getLifeTime()) {
                        particleWithMinLifeTime = particle;
                    }
                } else {
                    particleWithMinLifeTime = particle;
                }
            }
        }
        if (particleWithMinLifeTime != null) {
            particleWithMinLifeTime.setLife(x, y);
        }
    }

    public void doDraw(Canvas canvas) {
        for (Particle particle : particles) {
            if (particle.isLife()) {
                particle.doDraw(canvas);
            }
        }
    }

    public void updatePhysics(int altDelta) {
        for (Particle particle : particles) {
            if (particle.isLife()) {
                particle.updatePhysics(altDelta);
            }
        }
    }

    public void destroy() {
        particles = null;
        bitmap.recycle();
        bitmap = null;
    }
}
