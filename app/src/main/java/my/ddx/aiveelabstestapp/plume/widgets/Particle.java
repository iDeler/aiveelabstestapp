package my.ddx.aiveelabstestapp.plume.widgets;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * a
 * Created by deler on 13.09.15.
 */
class Particle {
    private final Paint paint;
    public int xpos;
    public int ypos;
    public float disappearing = 0.0f;
    private long lifeTime;
    private Bitmap bitmap;
    private long maxLifeTime;

    public Particle(long maxLifeTime, Bitmap bitmap) {
        this.lifeTime = 0;
        this.maxLifeTime = maxLifeTime;
        this.bitmap = bitmap;

        paint = new Paint();
        paint.setAlpha(100);
    }

    public void updatePhysics(int distChange) {
        lifeTime -= distChange;
        if (lifeTime <= 0) {
            lifeTime = 0;
        }
    }

    public void doDraw(Canvas canvas) {
        disappearing = (float) lifeTime / (float) maxLifeTime;
        paint.setAlpha((int) (255 * disappearing));
        disappearing = 0.5f + 0.5f * disappearing;
        Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        Rect dest = new Rect(0, 0, (int) ((float) bitmap.getWidth() * disappearing), (int) ((float) bitmap.getHeight() * disappearing));
        dest.offsetTo(xpos - dest.width() / 2, ypos - dest.height() / 2);
        canvas.drawBitmap(bitmap, src, dest, paint);
    }

    public boolean isLife() {
        return lifeTime > 0;
    }

    public long getLifeTime() {
        return lifeTime;
    }

    public void setLife(int x, int y) {
        xpos = x;
        ypos = y;
        lifeTime = maxLifeTime;
        disappearing = 1.0f;
    }
}
