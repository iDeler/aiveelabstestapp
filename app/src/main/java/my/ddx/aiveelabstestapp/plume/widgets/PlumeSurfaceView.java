package my.ddx.aiveelabstestapp.plume.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import my.ddx.aiveelabstestapp.R;

/**
 * a
 * Created by deler on 13.09.15.
 */
public class PlumeSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

    private PlumeSurfaceThread thread;
    private Bitmap bgBitmap;
    private ParticleSystem particleSystem;

    private long lastFrameTime;
    private int lastX, lastY;
    private int bgXOffset;
    private int bgYOffset;

    public PlumeSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public PlumeSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PlumeSurfaceView(Context context) {
        super(context);
        init();
    }

    private void init() {
        getHolder().addCallback(this);
        thread = new PlumeSurfaceThread(getHolder(), this);

        setFocusable(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (canvas != null) {
            canvas.drawRGB(0, 0, 0);
            canvas.drawBitmap(bgBitmap, bgXOffset, bgYOffset, null);

            particleSystem.updatePhysics((int) (System.currentTimeMillis() - lastFrameTime));
            particleSystem.doDraw(canvas);

            lastFrameTime = System.currentTimeMillis();
        }
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                particleSystem.setParticle((int) event.getX(), (int) event.getY());
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                if (lastX == (int) event.getX() && lastY == (int) event.getY()) {
                    return true;
                }
                int deltaX = (int) (event.getX() - lastX);
                int deltaY = (int) (event.getY() - lastY);
//              particleSystem.setParticle(lastX + deltaX / 4, lastY + deltaY / 4);
                particleSystem.setParticle(lastX + deltaX / 2, lastY + deltaY / 2);
//              particleSystem.setParticle(lastX + deltaX * 3 / 4, lastY + deltaY * 3 / 4);
                particleSystem.setParticle((int) event.getX(), (int) event.getY());
                break;
            }
        }

        lastX = (int) event.getX();
        lastY = (int) event.getY();
        return true;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Bitmap background = BitmapFactory.decodeResource(getResources(), R.drawable.img_bg);
        float scaleY = (float) background.getHeight() / (float) getHeight();
        float scaleX = (float) background.getWidth() / (float) getWidth();
        float scale = Math.min(scaleX, scaleY);
        int newWidth = Math.round(background.getWidth() / scale);
        int newHeight = Math.round(background.getHeight() / scale);

        bgXOffset = (getWidth() - newWidth) / 2;
        bgYOffset = (getHeight() - newHeight) / 2;
        bgBitmap = Bitmap.createScaledBitmap(background, newWidth, newHeight, true);

        Bitmap particleBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_circle_blur);

        particleSystem = new ParticleSystem(40, 250, particleBitmap);

        thread.start();
        lastFrameTime = System.currentTimeMillis();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        thread.shutdown();
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        bgBitmap.recycle();
        bgBitmap = null;

        particleSystem.destroy();
        particleSystem = null;
    }

    public class PlumeSurfaceThread extends Thread {

        private final SurfaceHolder surfaceHolder;
        private final PlumeSurfaceView surfaceView;
        private boolean running;

        public PlumeSurfaceThread(SurfaceHolder surfaceHolder, PlumeSurfaceView surfaceView) {
            this.surfaceHolder = surfaceHolder;
            this.surfaceView = surfaceView;
        }

        @Override
        public synchronized void start() {
            running = true;
            super.start();
        }

        @Override
        public void run() {
            while (running) {
                Canvas c = null;
                try {
                    c = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        surfaceView.onDraw(c);
                    }

                    sleep(17);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (c != null) {
                        surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
        }

        public void shutdown() {
            running = false;
        }
    }
}
